;; COMMON-LISP 

;; http://stackoverflow.com/questions/6003662/how-to-sort-a-list-with-sublists-common-lisp
(sort '(3 2 4 2 1) '>)

(defun BIG-NUM-LIST (&rest args)
  (let* (
         ( A args)         
         ( B (sort (cdr A) '<)) ;; 오름차순 (1 2 3 4 5...)
         (_t (- (list-length B) (car A)));; B갯수 - 첫입력갯수
         )
    (nthcdr _t B) ;; take 함수가 없어서  _t 를 구해 nthcdr를 사용했다.
    ))

;; TEST
(BIG-NUM-LIST 2 8 3 5 7 10)

;; 수학함수 정의
(defun infinite-PARABOLOID (x y)
  (+ (* x x) (* y y)))

;; 계산
(let* (
       ( LIST (BIG-NUM-LIST 2 3 5 10 8))
       (x      (car LIST))
       (y (car (cdr LIST))) ;; CLISP에서는 cdr로 얻은 리스트가 1개일땐 atom? 으로 용납 안됨 -_-;
       )
  (infinite-PARABOLOID x y) 
  )

