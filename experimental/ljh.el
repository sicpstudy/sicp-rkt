;; ELISP
;; sort함수가 없어서 직접 정의해 만들어 주어야함.
(defun quicksort (lst)
  "Implement the quicksort algorithm."
  (if (null lst) nil
    (let* ((spl (car lst))
           (rst (cdr lst))
           (smalp (lambda (x)
                    (< x spl))))
      (append (quicksort (remove-if-not smalp rst))
              (list spl)
              (quicksort (remove-if smalp rst))))))


(defun BIG-NUM-LIST (&rest args)
  (let* (
         (A args)
         (B (quicksort (cdr A)))     
         (_t (- (list-length B) (car A)));; B갯수 - 첫입력갯수
         )
    (nthcdr _t B) ;; CLISP 을 하다가 해결본 코드
    ))

;; TEST
(BIG-NUM-LIST 2 8 3 10)


;; 수학함수 정의
(defun infinite-PARABOLOID (x y)
  (+ (* x x) (* y y)))

;; 계산
(let* (  ( LIST (BIG-NUM-LIST 2 3 5 10 8))
         (x (car LIST))
         (y (cdr LIST))) ;; cdr은 원래 첫번째 아이템을 제외한 list를 리턴한다.
  (infinite-PARABOLOID x y) 
  )

