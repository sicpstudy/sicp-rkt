;; ==================== hy 0.11.1 버전
(defn BIG-NUM-LIST [  &rest args ] 
  (let [
        [ T (car args) ]
        [ B (sorted :reverse true (cdr args)) ]
        ]
    (list (take T B))
    ))

;; 함수 TEST
(BIG-NUM-LIST 2 8 3 10)

;; 수학 함수 정의
(defn infinite-PARABOLOID [x y]
  (+ (pow x 2) (pow y 2)) )

;; 문제 실행
(let [
      [  LIST (BIG-NUM-LIST 2 3 5 10 8) ]
      [ x (first LIST) ]
      [ y (second LIST) ]
      ]
  (infinite-PARABOLOID x y)
  )

;;=================================================


