#lang racket

(define (deep-reverse lst)
  (if (list? lst)
      (reverse (map deep-reverse lst))
      lst))


(define-struct mobile (left right))
(define-struct branch (length structure))

(define (total-weight m)
  (if (mobile? m)
      (+ (torque (mobile-left m))
         (torque (mobile-right m)))
      m))

(define (torque b)
  (* (branch-length b)
     (total-weight (branch-structure b))))

(define (balanced? m)
  (if (mobile? m)
      (and (balanced? (branch-structure (mobile-left m)))
           (balanced? (branch-structure (mobile-right m)))
           ( = (torque (mobile-left m))
               (torque (mobile-right m))))
      #t))
  
#|(balanced? (make-mobile
	    (make-branch 6
			 4)
	    (make-branch 2
			 (make-mobile (make-branch 2 3)
				      (make-branch 3 2)))))|#
