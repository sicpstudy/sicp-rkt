#lang racket

(define (fast-expt b n)
  (cond [(= n 0) 1]
	[(even? n) (sqr (fast-expt b (/ n 2)))]
	[else (* b (fast-expt b (- n 1)))]))

;; 연습문제 1.16
(define (fast-expt* b n)
  (define (iter b n result)
    (cond [(= n 0) result]
	  [(even? n) (iter (sqr b)
			   (/ n 2)
			   result)]
	  [else (iter b
		      (sub1 n)
		      (* b result))]))
  (iter b n 1))
	   
	
