#lang racket

(define (prime? n)
  (define (divides? a b)
    (= (remainder b a) 0))
  (define (smallest-divisor n)
    (find-divisor n 2))
  (define (find-divisor n test-divisor)
    (cond ((> (sqr test-divisor) n) n)
          ((divides? test-divisor n) test-divisor)
          (else (find-divisor n (+ test-divisor 1)))))
  (= n (smallest-divisor n)))


(define (timed-prime-test n)
  (newline)
  (display n)
  (newline)
  (define (start-prime-test n start-time)
    (if (prime? n)
        (report-prime (- (current-inexact-milliseconds) start-time))
        (display "Not a prime")))
  (define (report-prime elapsed-time)
    (display " *** ")
    (newline)
    (display elapsed-time))
  
  (start-prime-test n (current-inexact-milliseconds)))

(define (search-for-primes start end)
  (if (= start end)
       