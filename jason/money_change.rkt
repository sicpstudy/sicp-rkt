#lang racket

(define (count-change amount)
  (cc amount 8))

(define (cc amount kinds-of-money)
  (cond ((= amount 0) 1)
        ((or (< amount 0) (= kinds-of-money 0)) 0)
        (else (+ (cc amount
                     (- kinds-of-money 1))
                 (cc (- amount
                        (first-denomination kinds-of-money))
                     kinds-of-money)))))

(define (first-denomination kinds-of-money)
  (cond ((= kinds-of-money 1) 50000)
        ((= kinds-of-money 2) 10000)
        ((= kinds-of-money 3) 5000)
        ((= kinds-of-money 4) 1000)
        ((= kinds-of-money 5) 500)
        ((= kinds-of-money 6) 100)
        ((= kinds-of-money 7) 50)
        ((= kinds-of-money 8) 10)))
