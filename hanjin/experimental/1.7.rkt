#lang racket

(define error-rate 0.000000001)
(define (>= x y) (not (< x y)))
(define (average x y) (/ (+ x y) 2))

;;sqrt newton's method
(define (improve guess x)
  (average guess (/ x guess)))

;;error-rate calc
(define (good-enough? guess x)
  (and (>= guess 0)(< (abs(- guess x)) error-rate)))

;;sqrt-iter
(define (sqrt-iter prev guess x)
  (if (good-enough? prev guess)
      guess
      (sqrt-iter guess (improve guess x) x)))

;;sqrt
(define (sqrt x)
  (sqrt-iter 0 1.0 x))